import axios, { AxiosError } from "axios";

const isAuthenticated = async (): Promise<boolean> => {
  if (process.server) return true;
  const store = useUserDataStore();
  const config = useRuntimeConfig();
  const accessToken = process.client
    ? localStorage.getItem("access_token")
    : null;

  if (accessToken !== null) {
    try {
      console.log("checkLogin");
      const response = await axios.post(`${config.public.apiUrl}/check-login`, {
        auth_key: accessToken,
      });
      console.log("succes:", response.data);
      store.updateUserData(response.data);
      console.log(store.username);
      // localStorage.setItem("user_data", JSON.stringify(response.data));

      return true;
    } catch (error: unknown) {
      console.log("error:", error);
      const axiosError = error as AxiosError;
      console.log("failed check:", axiosError.response);
      return false;
    }
  } else {
    return false;
  }
};

export default defineNuxtRouteMiddleware(async (to, from) => {
  const status = await isAuthenticated(); // Wait for the authentication check
  if (status === false) {
    console.log("status", status);
    return navigateTo("/login");
  }
});
