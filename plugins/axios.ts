import axios from "axios";

export default defineNuxtPlugin((nuxtApp) => {
  const config = useRuntimeConfig();
  const defaultUrl = config.public.apiUrl;

  let api = axios.create({
    baseURL: defaultUrl,
    headers: {
      common: {
        "Content-Type": "application/json",
      },
    },
  });

  // Add request interceptor to handle errors or perform other tasks
  api.interceptors.request.use(
    (config) => {
      // You can modify the request config here
      if (config.baseURL && !config.baseURL.endsWith("/")) {
        config.baseURL += "/";
      }
      console.log("request_intercept:", config);
      return config;
    },
    (error) => {
      // Handle request errors here
      console.log("request_error:", error);
      return Promise.reject(error);
    }
  );

  // Add response interceptor to handle errors or perform other tasks
  api.interceptors.response.use(
    (response) => {
      // You can modify the response data here
      console.log("response_intercept:", response);
      return response;
    },
    (error) => {
      // Handle response errors here
      console.log("response_error:", error);
      return Promise.reject(error);
    }
  );

  return {
    provide: {
      api: api,
    },
  };
});
