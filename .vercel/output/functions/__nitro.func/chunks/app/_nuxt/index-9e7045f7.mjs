import { _ as _export_sfc, u as useRuntimeConfig, a as __nuxt_component_0 } from '../server.mjs';
import { _ as __nuxt_component_0$1 } from './index-094067bd.mjs';
import { _ as __nuxt_component_2 } from './index-aa053c9c.mjs';
import { ssrRenderComponent, ssrRenderAttrs, ssrRenderStyle, ssrRenderAttr } from 'vue/server-renderer';
import { useSSRContext, mergeProps, withCtx, createVNode } from 'vue';
import axios from 'axios';
import '../../nitro/vercel.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import './Icon-e9878bea.mjs';
import './config-7450675c.mjs';
import '@iconify/vue/dist/offline';
import '@iconify/vue';

const _sfc_main$1 = {
  data() {
    return {
      mediaStream: null,
      capturedImage: null
    };
  },
  methods: {
    async startCamera() {
      try {
        this.mediaStream = await navigator.mediaDevices.getUserMedia({
          video: true
        });
        this.$refs.video.srcObject = this.mediaStream;
      } catch (error) {
        console.error("Error accessing camera:", error);
      }
    },
    async takePhoto() {
      const canvas = document.createElement("canvas");
      canvas.width = this.$refs.video.videoWidth;
      canvas.height = this.$refs.video.videoHeight;
      canvas.getContext("2d").drawImage(this.$refs.video, 0, 0, canvas.width, canvas.height);
      const dataURL = canvas.toDataURL("image/jpeg");
      this.capturedImage = dataURL;
    },
    handleFileChange() {
      const file = this.$refs.fileInput.files[0];
      const reader = new FileReader();
      reader.onload = (e) => {
        this.capturedImage = e.target.result;
      };
      reader.readAsDataURL(file);
    }
  }
};
function _sfc_ssrRender$1(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  _push(`<div${ssrRenderAttrs(_attrs)}><video autoplay playsinline></video><button>Start Camera</button><button>Take Photo</button><input type="file" style="${ssrRenderStyle({ "display": "none" })}" accept="image/*">`);
  if ($data.capturedImage) {
    _push(`<img${ssrRenderAttr("src", $data.capturedImage)} alt="Captured Image">`);
  } else {
    _push(`<!---->`);
  }
  _push(`</div>`);
}
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/CameraCapture/index.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_3 = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["ssrRender", _sfc_ssrRender$1]]);
const _sfc_main = {
  middleware: "auth",
  data() {
    return {
      toastVisible: false,
      toastTitle: "",
      toastMessage: "",
      formTitle: "Profile Data",
      submitName: "Save",
      fields: [
        { id: "name", name: "Name", type: "text", value: "" },
        { id: "username", name: "Username", type: "text", value: "" },
        { id: "email", name: "Email", type: "email", value: "" },
        { id: "password", name: "Password", type: "password", value: "" }
      ],
      formData: {}
    };
  },
  mounted() {
    this.fetchData();
  },
  methods: {
    fetchData() {
      const config = /* @__PURE__ */ useRuntimeConfig();
      const userDataString = localStorage.getItem("user_data");
      const accessToken = localStorage.getItem("access_token");
      const userData = JSON.parse(userDataString);
      axios.get(`${config.public.apiUrl}/user/${userData == null ? void 0 : userData.id}`, {
        headers: {
          Authorization: `Bearer ${accessToken}`
        }
      }).then((response) => {
        this.formToField(response.data);
      }).catch((error) => {
        const detail = error.response.data.detail;
        let message = "";
        if (detail[0].msg) {
          message = detail[0].msg;
        } else {
          message = detail;
        }
        this.showToast("error", message);
      });
    },
    fieldsToForm(fields) {
      fields.forEach((field) => {
        if (this.formData.hasOwnProperty(field.id)) {
          this.formData[field.id] = field.value;
        }
      });
    },
    formToField(form) {
      this.fields.forEach((field) => {
        if (form.hasOwnProperty(field.id)) {
          if (field.id !== "password")
            field.value = form[field.id];
        }
      });
    },
    onSubmit(data) {
      const config = /* @__PURE__ */ useRuntimeConfig();
      axios.put(`${config.public.apiUrl}/user/`, data).then((response) => {
        this.showToast("success", "Account created successfully");
        this.$router.push("/login");
      }).catch((error) => {
        const detail = error.response.data.detail;
        let message = "";
        if (detail[0].msg) {
          message = detail[0].msg;
        } else {
          message = detail;
        }
        this.showToast("error", message);
      });
    },
    showToast(title, message) {
      this.toastTitle = title;
      this.toastMessage = message;
      this.toastVisible = true;
      setTimeout(() => {
        this.toastVisible = false;
      }, 2e3);
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_NuxtLayout = __nuxt_component_0;
  const _component_Toast = __nuxt_component_0$1;
  const _component_DynamicForm = __nuxt_component_2;
  const _component_CameraCapture = __nuxt_component_3;
  _push(ssrRenderComponent(_component_NuxtLayout, mergeProps({ name: "dashboard" }, _attrs), {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<div class="flex justify-evenly container p-8"${_scopeId}>`);
        _push2(ssrRenderComponent(_component_Toast, {
          visible: $data.toastVisible,
          title: $data.toastTitle,
          message: $data.toastMessage
        }, null, _parent2, _scopeId));
        _push2(`<div class="p-2 rounded-lg lg:w-1/3 xl:1/2 h-fit border-solid border-slate-300 border-2"${_scopeId}>`);
        _push2(ssrRenderComponent(_component_DynamicForm, {
          fields: $data.fields,
          formTitle: $data.formTitle,
          submitName: $data.submitName,
          onSubmit: $options.onSubmit
        }, null, _parent2, _scopeId));
        _push2(`</div><div class="p-2 rounded-lg lg:w-1/3 xl:1/2 h-fit border-solid border-slate-300 border-2"${_scopeId}>`);
        _push2(ssrRenderComponent(_component_CameraCapture, null, null, _parent2, _scopeId));
        _push2(`</div></div>`);
      } else {
        return [
          createVNode("div", { class: "flex justify-evenly container p-8" }, [
            createVNode(_component_Toast, {
              visible: $data.toastVisible,
              title: $data.toastTitle,
              message: $data.toastMessage
            }, null, 8, ["visible", "title", "message"]),
            createVNode("div", { class: "p-2 rounded-lg lg:w-1/3 xl:1/2 h-fit border-solid border-slate-300 border-2" }, [
              createVNode(_component_DynamicForm, {
                fields: $data.fields,
                formTitle: $data.formTitle,
                submitName: $data.submitName,
                onSubmit: $options.onSubmit
              }, null, 8, ["fields", "formTitle", "submitName", "onSubmit"])
            ]),
            createVNode("div", { class: "p-2 rounded-lg lg:w-1/3 xl:1/2 h-fit border-solid border-slate-300 border-2" }, [
              createVNode(_component_CameraCapture)
            ])
          ])
        ];
      }
    }),
    _: 1
  }, _parent));
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/profile/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const index = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { index as default };
//# sourceMappingURL=index-9e7045f7.mjs.map
