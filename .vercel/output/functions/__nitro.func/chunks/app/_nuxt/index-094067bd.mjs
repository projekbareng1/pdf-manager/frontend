import __nuxt_component_0$1 from './Icon-e9878bea.mjs';
import { mergeProps, useSSRContext } from 'vue';
import { ssrRenderAttrs, ssrRenderClass, ssrRenderComponent, ssrInterpolate } from 'vue/server-renderer';
import { _ as _export_sfc } from '../server.mjs';

const _sfc_main = {
  props: {
    type: String,
    // as PropType<Type>,
    message: String,
    visible: Boolean
  },
  computed: {
    color() {
      return this.type === "success" ? "green" : this.type === "danger" ? "red" : "orange";
    },
    icon() {
      return this.type === "success" ? "icon-park-twotone:success" : this.type === "danger" ? "ic:twotone-dangerous" : "ic:twotone-warning";
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_Icon = __nuxt_component_0$1;
  if ($props.visible) {
    _push(`<div${ssrRenderAttrs(mergeProps({
      id: "toast-success",
      class: "fixed flex items-center w-full max-w-xs p-4 mb-4 text-gray-500 bg-white rounded-lg shadow top-5 right-5",
      role: "alert"
    }, _attrs))}><div class="${ssrRenderClass(`inline-flex items-center justify-center flex-shrink-0 w-8 h-8 text-${$options.color}-500 bg-${$options.color}-100 rounded-lg `)}">`);
    _push(ssrRenderComponent(_component_Icon, {
      size: "25",
      class: "w-full h-full",
      name: $options.icon
    }, null, _parent));
    _push(`</div><div class="ml-3 text-sm font-normal">${ssrInterpolate($props.message)}</div><button type="button" class="ml-auto -mx-1.5 -my-1.5 bg-white text-gray-400 hover:text-gray-900 rounded-lg focus:ring-2 focus:ring-gray-300 p-1.5 hover:bg-gray-100 inline-flex items-center justify-center h-8 w-8" data-dismiss-target="#toast-success" aria-label="Close"><span class="sr-only">Close</span><svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14"><path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"></path></svg></button></div>`);
  } else {
    _push(`<!---->`);
  }
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/Toast/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const __nuxt_component_0 = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { __nuxt_component_0 as _ };
//# sourceMappingURL=index-094067bd.mjs.map
