import { _ as _export_sfc, a as __nuxt_component_0 } from '../server.mjs';
import { _ as __nuxt_component_2 } from './index-aa053c9c.mjs';
import { _ as __nuxt_component_0$1 } from './nuxt-link-faa5b7b9.mjs';
import { u as useToastStore } from './store-946753a9.mjs';
import { mergeProps, withCtx, createTextVNode, createVNode, useSSRContext } from 'vue';
import { ssrRenderComponent, ssrRenderStyle } from 'vue/server-renderer';
import '../../nitro/vercel.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import 'axios';

const _sfc_main = {
  data() {
    return {
      formTitle: "Login",
      submitName: "Login",
      fields: [
        { id: "username", name: "Username", type: "username", value: "" },
        { id: "password", name: "Password", type: "password", value: "" }
      ]
    };
  },
  methods: {
    onSubmit(data) {
      const toastStore = useToastStore();
      this.$api.post(`/login/`, data).then((response) => {
        console.log(response.data.access_token);
        localStorage.setItem("access_token", response.data.access_token);
        toastStore.showToast("Login successfull", "success");
        this.$router.push("/dashboard");
      }).catch((error) => {
        const detail = error.response.data.detail;
        let message = "";
        if (detail[0].msg) {
          message = detail[0].msg;
        } else {
          message = detail;
        }
        toastStore.showToast(message, "danger");
      });
    }
  }
};
function _sfc_ssrRender(_ctx, _push, _parent, _attrs, $props, $setup, $data, $options) {
  const _component_NuxtLayout = __nuxt_component_0;
  const _component_DynamicForm = __nuxt_component_2;
  const _component_NuxtLink = __nuxt_component_0$1;
  _push(ssrRenderComponent(_component_NuxtLayout, mergeProps({ name: "auth" }, _attrs), {
    default: withCtx((_, _push2, _parent2, _scopeId) => {
      if (_push2) {
        _push2(`<div class="w-full mx-12 max-w-xl lg:w-3/4 lg:max-w-4xl bg-transparent flex"${_scopeId}><div class="hidden lg:block p-8 lg:rounded-tl-3xl shadow-xl w-1/2 bg-slate-400 bg-blend-darken mt-10" style="${ssrRenderStyle({ "background-image": "url('/assets/img/auth/banner.jpg')" })}"${_scopeId}><h1 class="text-5xl text-slate-50"${_scopeId}>Aplikasi Manajemen PDF</h1></div><div class="w-full lg:w-1/2 bg-white p-8 lg:rounded-br-3xl shadow-xl mx-auto mb-10"${_scopeId}><h1 class="text-4xl mb-8 text-center text-slate-900 lg:hidden"${_scopeId}> Aplikasi Manajemen PDF </h1>`);
        _push2(ssrRenderComponent(_component_DynamicForm, {
          fields: $data.fields,
          formTitle: $data.formTitle,
          submitName: $data.submitName,
          onSubmit: $options.onSubmit
        }, null, _parent2, _scopeId));
        _push2(`<hr class="my-2"${_scopeId}><div class="mb-4 w-full flex"${_scopeId}><div class="w-1/2"${_scopeId}><span class="block"${_scopeId}> Don&#39;t have account? `);
        _push2(ssrRenderComponent(_component_NuxtLink, {
          class: "block text-blue-500",
          to: "/register"
        }, {
          default: withCtx((_2, _push3, _parent3, _scopeId2) => {
            if (_push3) {
              _push3(`Register`);
            } else {
              return [
                createTextVNode("Register")
              ];
            }
          }),
          _: 1
        }, _parent2, _scopeId));
        _push2(`</span></div><div class="w-1/2"${_scopeId}><span class="block"${_scopeId}><a class="block text-right text-yellow-500" href="#"${_scopeId}>Forgot password?</a></span></div></div></div></div>`);
      } else {
        return [
          createVNode("div", { class: "w-full mx-12 max-w-xl lg:w-3/4 lg:max-w-4xl bg-transparent flex" }, [
            createVNode("div", {
              class: "hidden lg:block p-8 lg:rounded-tl-3xl shadow-xl w-1/2 bg-slate-400 bg-blend-darken mt-10",
              style: { "background-image": "url('/assets/img/auth/banner.jpg')" }
            }, [
              createVNode("h1", { class: "text-5xl text-slate-50" }, "Aplikasi Manajemen PDF")
            ]),
            createVNode("div", { class: "w-full lg:w-1/2 bg-white p-8 lg:rounded-br-3xl shadow-xl mx-auto mb-10" }, [
              createVNode("h1", { class: "text-4xl mb-8 text-center text-slate-900 lg:hidden" }, " Aplikasi Manajemen PDF "),
              createVNode(_component_DynamicForm, {
                fields: $data.fields,
                formTitle: $data.formTitle,
                submitName: $data.submitName,
                onSubmit: $options.onSubmit
              }, null, 8, ["fields", "formTitle", "submitName", "onSubmit"]),
              createVNode("hr", { class: "my-2" }),
              createVNode("div", { class: "mb-4 w-full flex" }, [
                createVNode("div", { class: "w-1/2" }, [
                  createVNode("span", { class: "block" }, [
                    createTextVNode(" Don't have account? "),
                    createVNode(_component_NuxtLink, {
                      class: "block text-blue-500",
                      to: "/register"
                    }, {
                      default: withCtx(() => [
                        createTextVNode("Register")
                      ]),
                      _: 1
                    })
                  ])
                ]),
                createVNode("div", { class: "w-1/2" }, [
                  createVNode("span", { class: "block" }, [
                    createVNode("a", {
                      class: "block text-right text-yellow-500",
                      href: "#"
                    }, "Forgot password?")
                  ])
                ])
              ])
            ])
          ])
        ];
      }
    }),
    _: 1
  }, _parent));
}
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/login/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const index = /* @__PURE__ */ _export_sfc(_sfc_main, [["ssrRender", _sfc_ssrRender]]);

export { index as default };
//# sourceMappingURL=index-acfb150e.mjs.map
