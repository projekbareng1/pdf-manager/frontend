import { g as defineStore } from '../server.mjs';

const useToastStore = defineStore("toast", {
  state: () => ({
    message: "",
    type: "",
    visible: false,
    visibleTime: 2e3
  }),
  getters: {
    isVisible: (state) => state.visible
  },
  actions: {
    showToast(message, type) {
      this.message = message;
      this.type = type;
      this.visible = true;
      setTimeout(() => {
        this.message = "";
        this.type = "";
        this.visible = false;
      }, this.visibleTime);
    }
  }
});

export { useToastStore as u };
//# sourceMappingURL=store-946753a9.mjs.map
