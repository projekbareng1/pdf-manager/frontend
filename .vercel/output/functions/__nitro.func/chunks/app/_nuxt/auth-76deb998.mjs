import { _ as __nuxt_component_0 } from './index-094067bd.mjs';
import { u as useToastStore } from './store-946753a9.mjs';
import { defineComponent, mergeProps, unref, useSSRContext } from 'vue';
import { ssrRenderAttrs, ssrRenderComponent, ssrRenderSlot } from 'vue/server-renderer';
import './Icon-e9878bea.mjs';
import '../server.mjs';
import '../../nitro/vercel.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import 'axios';
import './config-7450675c.mjs';
import '@iconify/vue/dist/offline';
import '@iconify/vue';

const _sfc_main = /* @__PURE__ */ defineComponent({
  __name: "auth",
  __ssrInlineRender: true,
  setup(__props) {
    const store = useToastStore();
    return (_ctx, _push, _parent, _attrs) => {
      const _component_Toast = __nuxt_component_0;
      _push(`<div${ssrRenderAttrs(mergeProps({
        class: "min-h-screen flex items-center justify-center bg-gray-100",
        style: { "background-image": "url('/assets/img/auth/bg.jpg')" }
      }, _attrs))}>`);
      _push(ssrRenderComponent(_component_Toast, {
        visible: unref(store).isVisible,
        message: unref(store).message,
        type: unref(store).type
      }, null, _parent));
      _push(`<div class="flex">`);
      ssrRenderSlot(_ctx.$slots, "default", { class: "bg-slate-500" }, null, _push, _parent);
      _push(`</div></div>`);
    };
  }
});
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("layouts/auth.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=auth-76deb998.mjs.map
